

/**
 * Created by ToxaLife on 04/05/15.
 */
class PriorityQueue[ K <% Ordered[K], V]( val count: Int,
                                          val head: Option[Node[K, V]]) {

  def this() {
    this(0, None)
  }

  def isEmpty(): Boolean = {
    (count == 0)
  }

  def getMaximum():(Option[K], Option[V]) = head match {
    case Some(head) => (Option(head.key),Option(head.value))
    case None       => (None, None)
  }

  def normalize(curNode: Node[K, V]) : Option[Node[K, V]] = curNode.left match {
    case None => None
    case Some(leftNode) => curNode.right match {
      case None => Option(new Node(normalize(leftNode), None, leftNode.key, leftNode.value, curNode.countL - 1, curNode.countR))
      case Some(rightNode) => {
        if (leftNode.key >= rightNode.key) {
          Option(new Node(normalize(leftNode), curNode.right, leftNode.key, leftNode.value, curNode.countL - 1, curNode.countR))
        } else {
          Option(new Node(curNode.left, normalize(rightNode), rightNode.key, rightNode.value, curNode.countL, curNode.countR - 1))
        }
      }
    }
  }
  def extractMax(): (PriorityQueue[K, V], (Option[K], Option[V])) = head match {
    case None => (this, (None, None))
    case Some(head1) => {
      val maximum = getMaximum()
      val newHead = normalize(head1)
//      println("extracted maximum:\t" + maximum)
      (new PriorityQueue[K, V](count - 1, newHead), maximum)
    }
  }

  def isNotFilled(count: Int): Boolean = count match {//true - non-finished, false - done
    case 0 => true
    case _ => {
      val c = math.log(count + 1) / math.log(2.0)
      (c != Math.floor(c))
    }
  }

  def findCorrectPlace(curNode: Node[K, V], parentNode: Option[Node[K, V]]) : Node[K, V] = parentNode match {
    case None => curNode
    case Some(p) => (curNode.key <= p.key) match {
      case true => {

        if (isNotFilled(p.countL))
          new Node(Option(findCorrectPlace(curNode, p.left)), p.right, p.key, p.value, p.countL + 1, p.countR)
        else if (isNotFilled(p.countR))
          new Node(p.left, Option(findCorrectPlace(curNode, p.right)), p.key, p.value, p.countL, p.countR + 1)
        else if (p.countL == p.countR)
          new Node(Option(findCorrectPlace(curNode, p.left)), p.right, p.key, p.value, p.countL + 1, p.countR)
        else
          new Node(p.left, Option(findCorrectPlace(curNode, p.right)), p.key, p.value, p.countL, p.countR + 1)

      }
      case false => {
        if (isNotFilled(p.countL))
          new Node(Option(findCorrectPlace(new Node[K, V](None, None, p.key, p.value, 0, 0), p.left)), p.right, curNode.key, curNode.value, p.countL + 1, p.countR)
        else if (isNotFilled(p.countR))
          new Node(p.left, Option(findCorrectPlace(new Node[K, V](None, None, p.key, p.value, 0, 0), p.right)), curNode.key, curNode.value, p.countL, p.countR + 1)
        else if (p.countL == p.countR)
          new Node(Option(findCorrectPlace(new Node[K, V](None, None, p.key, p.value, 0, 0), p.left)), p.right, curNode.key, curNode.value, p.countL + 1, p.countR)
        else
          new Node(p.left, Option(findCorrectPlace(new Node[K, V](None, None, p.key, p.value, 0, 0), p.left)), curNode.key, curNode.value, p.countL, p.countR + 1)
      }
    }
  }


  def insertElement(k: K, v: V) : PriorityQueue[K, V] = {
    val newVertex = new Node[K, V](None,None, k, v, 0, 0)
    val newHead = findCorrectPlace(newVertex, head)
    new PriorityQueue[K, V](count + 1, Option(newHead))
  }

  def print() : Unit = {
      print1(head, -1)
      println(count)
      println("---------")
  }
  val print1: (Option[Node[K, V]], Int) => Unit = {
    case (None,_)  => Nil
    case (Some(node), level) => {
      tab(level)
      //if (level != -1)
        println(node.key, node.value)
      if (node.left != None) {
        print1(node.left, level + 1)
      }
      if (node.right != None) {
        print1(node.right, level + 1)
      }
    }
  }
  def tab(level: Int){
    if (level <0) return
    printf("\t")
    tab(level-1)
  }
}

