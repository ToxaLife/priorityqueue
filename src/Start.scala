import scala.runtime.RichInt

/**
 * Created by toxalife on 19/05/15.
 */
object Start extends App{
  println("Lab 4: priority queue")
  var myPQ = new PriorityQueue[Double, String]()
  var maxPair: (Option[Double], Option[String]) = _
  println(myPQ.isEmpty())
  println(myPQ.getMaximum())
  myPQ = myPQ.insertElement(14.1, "a")
  println("current Maximum:\t" + myPQ.getMaximum())
//  (myPQ, maxPair) = myPQ.extractMax()
  myPQ = myPQ.insertElement(14.2, "b")
  val (myPQ_temp, maxPair_temp) = myPQ.extractMax()
  myPQ = myPQ_temp
  maxPair = maxPair_temp
  println(maxPair)
  myPQ = myPQ.insertElement(8, "str")
  myPQ = myPQ.insertElement(21, "scala")
  myPQ = myPQ.insertElement(16, "asd")
  myPQ = myPQ.insertElement(7, "asd")
  myPQ = myPQ.insertElement(9, "pp")
  myPQ = myPQ.insertElement(8, "asd")
  myPQ = myPQ.insertElement(9, "2")
  myPQ = myPQ.insertElement(10, "10")
  myPQ = myPQ.insertElement(11, "none")
  myPQ = myPQ.insertElement(12, "asd")
  myPQ = myPQ.insertElement(13, "frf")
  myPQ = myPQ.insertElement(14, "rt")
  //

//  println("current Maximum:\t" + myPQ.getMaximum())
  val (myPQ_temp1, maxPair_temp1) = myPQ.extractMax()
  myPQ = myPQ_temp1
  maxPair = maxPair_temp1
  println(maxPair)
  println("current Maximum:\t" + myPQ.getMaximum())
  println("printing")
  myPQ.print()
  println("Finished");
}
