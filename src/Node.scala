/**
 * Created by toxalife on 26/05/15.
 */
class Node[K, V]( val left: Option[Node[K, V]],
                  val right: Option[Node[K, V]],
                  val key: K,
                  val value: V,
                  val countL: Integer,
                  val countR: Integer){
  def this(k: K, v: V) = this(None, None, k, v, 0, 0)
}
